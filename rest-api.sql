-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2020 at 05:23 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rest-api`
--

-- --------------------------------------------------------

--
-- Table structure for table `major`
--

CREATE TABLE `major` (
  `id` int(11) NOT NULL,
  `major` varchar(50) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `major`
--

INSERT INTO `major` (`id`, `major`, `active`) VALUES
(1, 'Information Technology', 1),
(2, 'Bussiness Information', 1);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `fname` varchar(60) NOT NULL,
  `lname` varchar(60) NOT NULL,
  `email` varchar(30) NOT NULL,
  `year` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `major` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `f_name` varchar(255) DEFAULT NULL,
  `l_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `fname`, `lname`, `email`, `year`, `address`, `major`, `active`, `f_name`, `l_name`) VALUES
(1, 'Sandy', 'Sandoro', 'sandysandoro@gmail.com', 1990, 'Jl. Maju Mundur No. 70 Semarang Jawa Tengah', 1, 1, NULL, NULL),
(2, 'SpongeBobs', 'SquarePants', 'sponge.bob@gmail.com', 2001, 'Jl. Bikini Botom 21 Atlantis', 1, 1, NULL, NULL),
(3, 'Fira', 'Andriani', 'fandriani@yahoo.com', 2000, 'Jl Blimbing No 21 Malang', 1, 1, NULL, NULL),
(4, 'Son', 'Goku', 'son.goku@gmail.com', 2004, 'Jl. Veteran 30 Surabaya', 1, 0, NULL, NULL),
(5, 'Los', 'Dol', 'losdol@gmail.com', 2010, 'asdqwe rty', 1, 0, NULL, NULL),
(17, 'Andi', 'Bagaskoro', 'andi.bg@gmail.com', 2000, 'Jl. Orangutan 21 Jakarta', 1, 0, NULL, NULL),
(18, 'Booby', 'Dray', 'b.dray@gmail.com', 2002, 'Jl Bingung 22 Bintaro', 2, 0, NULL, NULL),
(20, 'Lovely', 'Candy', 'lcandy@gmail.com', 2000, 'Jl Kamasutra 22 Jagorawi', 1, 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `major`
--
ALTER TABLE `major`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `major`
--
ALTER TABLE `major`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
