package com.test.restapi.Repository;

import com.test.restapi.Entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Optional;

public class StudentImpl {//implements StudentRepository{
    /*@Autowired
    private JdbcTemplate jdbc;

    //@Override
    //public int count() {
    //    return jdbc.queryForObject("SELECT COUNT(*) FROM student", Integer.class);
    //}

    @Override
    public Student save(Student student) {
        int result = jdbc.update(
                "INSERT INTO student(fname, lname, email, year, address, major) values (?,?,?,?,?,?)",
                student.getFname(), student.getLname(), student.getEmail(), student.getYear(), student.getAddress(), student.getMajor()
        );
        return student;
    }

    //@Override
    //public int update(Student student) {
    //    return 0;
    //}


    @Override
    public void delete(Student student) {
        int result = jdbc.update("DELETE FROM student WHERE id =?", student.getId());
        //return student;
    }



    @Override
    public List<Student> findAll() {
        return jdbc.query(
                "SELECT * FROM student",
                (rs, rowNum)->new Student(
                        rs.getInt("id"),
                        rs.getString("fname"),
                        rs.getString("lname"),
                        rs.getString("email"),
                        rs.getInt("year"),
                        rs.getString("address"),
                        rs.getInt("major"),
                        rs.getInt("active")
                ));
    }

    @Override
    public List<Student> findByfname(String name) {
        return null;
    }

    @Override
    public Optional<Student> findById(int id) {
        return Optional.empty();
    }

     */
}
