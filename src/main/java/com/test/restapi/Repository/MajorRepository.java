package com.test.restapi.Repository;

import com.test.restapi.Entity.Major;
import com.test.restapi.Entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MajorRepository extends JpaRepository<Major, Integer> {
    List<Major> findAll();
}
