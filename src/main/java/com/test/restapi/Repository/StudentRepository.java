package com.test.restapi.Repository;

import com.test.restapi.Entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Integer> {
    //int count();
    Student save(Student student);
    //int update(Student student);
    void delete(Student student);


    List<Student> findAll();
    List<Student> findByfname(String name);
    Optional<Student> findById(int id);
}
