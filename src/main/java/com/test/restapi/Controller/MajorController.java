package com.test.restapi.Controller;

import com.test.restapi.Entity.Major;
import com.test.restapi.Repository.MajorRepository;
import com.test.restapi.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MajorController {
    @Autowired
    MajorRepository majrep;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/majors")
    public List<Major> findAllMajors(){
        return majrep.findAll();
    }
}
