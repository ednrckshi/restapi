package com.test.restapi.Controller;

import com.test.restapi.Entity.Major;
import com.test.restapi.Entity.Student;
import com.test.restapi.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@RestController
public class StudentController {
    @GetMapping("/HelloWorld")
    public String HelloWorld(){
        return "Hello World";
    }

    @GetMapping("/HelloWorldName/{name}")
    public String HelloWorldName(@PathVariable(value = "name") String name){
        return "Hello " + name;
    }

    @PostMapping("/HelloWorldId")
    public String HelloWorldPost(@RequestParam(name = "username") String username){
        return "Hello " + username;
    }

    @PostMapping("/student")
    public Student HelloWorldUserPostController(@RequestBody Student student){
        return student;
    }

    @Autowired
    StudentRepository sturep;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/students")
    public List<Student> findAllStudents(){
        return sturep.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/students/{id}")
    public Optional<Student> findStudent(@PathVariable(value = "id") int id){
        return sturep.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/students")
    public Student save(@RequestBody Student student){
        return sturep.save(student);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/students/{id}")
    public Student save(@PathVariable(value = "id") int id, @RequestBody Student student){
        return sturep.save(student);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/students/{id}")
    public void delete(@PathVariable(value = "id") int id){
        Optional<Student> student = sturep.findById(id);
        if(student.isPresent()) {
            sturep.delete(student.get());
            //return student.get();
        }
        //return new Student();
    }


    RestTemplate resttemp = new RestTemplate();

    @GetMapping("/todo/get")
    public String getTodo(){
        String result = resttemp.getForObject("https://jsonplaceholder.typicode.com/todos", String.class);
        return result;
    }



}
