package com.test.restapi.Entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    @Id
    private int id;

    @Size(min=3, max=50, message="name error 3-50char")
    private String fname;
    private String lname;

    @Email(message="invalid email")
    private String email;

    private int year;
    private String address;
    private int major;
    private int active;
}
