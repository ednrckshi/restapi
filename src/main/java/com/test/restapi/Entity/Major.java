package com.test.restapi.Entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Major {
    @Id
    private int id;

    private String major;
    private int active;

}
